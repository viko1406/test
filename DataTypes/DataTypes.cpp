// DataTypes.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cstdlib>
#include <string>
#include <iomanip>
#include <sstream>

bool equal(float value, float reference, float tolerance)
{
    return !(value < reference - tolerance) && !(value > reference + tolerance);
}

void Speedometer()
{
    char text[50];
    float current_speed = 0.0f;

    float delta_speed = 0.0f;

    do
    {
        std::cout << "Please, type speed difference: ";
        std::cin >> delta_speed;

        current_speed = current_speed + delta_speed;
        if (current_speed > 150.f)
        {
            current_speed = 150.f;
        }
        else if (current_speed < 0 || equal(current_speed, 0.f, 0.01f))
        {
            current_speed = 0.f;
            break;
        }


        sprintf_s(text, "Your current speed: %.1f km/h \n", current_speed);
        std::cout << text;
    }
    while (1);

    std::cout << "You have stopped!";
}

void Stapler()
{
    std::string whole_part;
    std::string fractional_part;

    std::cout << "Please, input whole part of number: ";
    std::cin >> whole_part;
    std::cout << "Please, input fractional part of number: ";
    std::cin >> fractional_part;

    std::string result = whole_part + "." + fractional_part;

    double final_number = std::stod(result);

    std::cout << "Result " << std::fixed << std::setprecision(fractional_part.size())<< final_number << std::endl;
    std::cout << std::defaultfloat;
}

void Calculator()
{
    std::string input_string;
    std::cout << "Please, input string (<value1><operator><value2>): ";
    std::cin >> input_string;

    double first_number;
    double second_number;
    

    for (size_t i = 0; i < input_string.size(); i++)
    {
        if (input_string[i] == '+' || input_string[i] == '-' || input_string[i] == '*' || input_string[i] == '/')
        {
            first_number = std::stod(input_string.substr(0, i));
            second_number = std::stod(input_string.substr(i + 1, input_string.size() - i));

            double result;

            switch (input_string[i])
            {
                case '+':
                {
                    result = first_number + second_number;
                    break;
                }
                case '-':
                {
                    result = first_number - second_number;
                    break;
                }
                case '*':
                {
                    result = first_number * second_number;
                    break;
                }
                case '/':
                {
                    result = first_number / second_number;
                    break;
                }
            }

            std::cout << "Result of calculation: " << first_number << " " << input_string[i] << " " << second_number << " = " << result;

            break;
        }
        else if (input_string.size() - 1 == i || input_string[i] == ' ')
        {
            std::cout << "Invalid format, please, use this one: <value1><operator><value2>";
        }
    }
}

enum Note
{
    DO = 1,
    RE = 2,
    MI = 4,
    FA = 8,
    SOL = 16,
    LA = 32,
    SI = 64
};

void Piano()
{
    const int melody_count = 12;

    std::string melody;
    std::string music;

    for (int i = 0; i < melody_count; i++)
    {
        do
        {
            std::cout << "Please, input melody (3 musical notes): ";
            std::cin >> melody;
            if (melody.size() != 3)
            {
                std::cout << "Need to input 3 notes!" << std::endl;
            }
            else
            {
                break;
            }
        } while (1);
        music = music + melody + " ";
    }

    for (int i = 0; i < music.size(); i++)
    {
        if (music[i] == ' ')
        {
            continue;
        }

        std::string note(music, i , 1);

        int note_index = std::stoi(note) - 1;

        if (note_index > 6 || note_index < 0)
        {
            continue;
        }

        switch(1 << note_index)
        {
            case DO:
            {
                std::cout << "DO";
                break;
            };
            case RE:
            {
                std::cout << "RE";
                break;
            };
            case MI:
            {
                std::cout << "MI";
                break;
            };
            case FA:
            {
                std::cout << "FA";
                break;
            };
            case SOL:
            {
                std::cout << "SOL";
                break;
            };
            case LA:
            {
                std::cout << "LA";
                break;
            };
            case SI:
            {
                std::cout << "SI";
                break;
            };
        }

        std::cout << " ";
    }

}

enum PowerLine
{
    OverallPower = 1,
    Sockets = 2,
    IndoorLight = 4,
    OutdoorLight = 8,
    IndoorHeating = 16,
    WaterPipeHeating = 32,
    AirConditioning = 64
};

bool StatusChanged(int current_status, int prev_status, PowerLine line)
{
    return (current_status ^ prev_status) & line;
}

void SmartHouse()
{
    int current_hour = 0;
    const int _2_days_in_hours = 24 * 2;

    const int max_color_temperature = 5000;
    const int min_color_temperature = 2700;
    int current_color_temperature = max_color_temperature;

    const int time_for_max_color_temp = 16;
    const int time_for_min_color_temp = 20;

    int delta_color_temp = (max_color_temperature - min_color_temperature) / (abs(time_for_min_color_temp - time_for_max_color_temp));

    int status = 0;
    int previous_status = 0;

    for (; current_hour < _2_days_in_hours; current_hour++)
    {
        std::cout << std::endl << std::endl << "Current time - " << current_hour % 24 << ":00 day - " << current_hour / 24 + 1 << std::endl;
        std::string input_data;
        std::cout << "Please, input data in format <outdoor_temperature> <indoor_temperature> <is there any movement outdoor(yes/no)> <is light on(on/off)>" << std::endl;
        
        std::cin.get(); //way to remove character 'Enter' from previous cin
        std::getline(std::cin, input_data);

        std::stringstream input_stream(input_data.c_str());

        std::string movement;
        std::string light_on;
        int outdoor_temperature;
        int indoor_temperature;

        input_stream >> outdoor_temperature >> indoor_temperature >> movement >> light_on;

        if (outdoor_temperature < 0)
        {
            status |= WaterPipeHeating;
        }
        else if (outdoor_temperature > 5)
        {
            status &= (~WaterPipeHeating);
        }

        if ((current_hour % 24 > 16 || current_hour % 24 < 5) && movement == "yes")
        {
            status |= OutdoorLight;
        }
        else
        {
            status &= (~OutdoorLight);
        }

        if (indoor_temperature < 22)
        {
            status |= IndoorHeating;
        }
        else if (indoor_temperature >= 25)
        {
            status &= (~IndoorHeating);
        }

        if (indoor_temperature > 30)
        {
            status |= AirConditioning;
        }
        else if (indoor_temperature <= 25)
        {
            status &= (~AirConditioning);
        }

        if (light_on == "on")
        {
            status |= IndoorLight;
        }
        else
        {
            status &= (~IndoorLight);
        }

        if (current_hour % 24 > 16 && current_hour % 24 <= 20)
        {
            current_color_temperature -= delta_color_temp;
        }

        if (current_hour % 24 == 0)
        {
            current_color_temperature = max_color_temperature;
        }


        if (StatusChanged(status, previous_status, IndoorLight))
        {
            std::cout << "Indoor light is: " << light_on;
            
        }
        if (status & IndoorLight)
        {
            std::cout << " Current color temperature: " << current_color_temperature << std::endl;
        }
        if (StatusChanged(status, previous_status, OutdoorLight))
        {
            std::cout << "Outdoor light is : " << ((status & OutdoorLight) ? "On":"Off") << std::endl;
        }
        if (StatusChanged(status, previous_status, IndoorHeating))
        {
            std::cout << "Indoor heating is :" << ((status & IndoorHeating) ? "On" : "Off") << std::endl;
        }
        if (StatusChanged(status, previous_status, WaterPipeHeating))
        {
            std::cout << "Water pipe heating is :" << ((status & WaterPipeHeating) ? "On" : "Off") << std::endl;
        }
        if (StatusChanged(status, previous_status, AirConditioning))
        {
            std::cout << "Air conditioning is :" << ((status & AirConditioning) ? "On" : "Off") << std::endl;
        }

        previous_status = status;
    }
}


int main()
{
    std::cout << "Part 1 - Speedometer" << std::endl;
    Speedometer();
    
    std::cout << std::endl << "Part 2 - Stapler" << std::endl;
    Stapler();
    
    std::cout << std::endl << "Part 3 - Calculator" << std::endl;
    Calculator();
    
    std::cout << std::endl << "Part 4 - Piano" << std::endl;
    Piano();
    
    std::cout << std::endl << "Part 5 - Smart House" << std::endl;
    SmartHouse();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
